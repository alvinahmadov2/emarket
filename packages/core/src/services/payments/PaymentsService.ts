import { injectable }                from 'inversify';
import Stripe, { ICard, customers }  from 'stripe';
import { v4 as uuid }                from 'uuid';
import Logger                        from 'bunyan';
import { YooCheckout }               from '@a2seven/yoo-checkout';
import {
	IItemWithoutData,
	ICheckoutCustomer,
	ICreatePayment
}                                    from "@a2seven/yoo-checkout/lib/types";
import { asyncListener, routerName } from '@pyro/io';
import Customer                      from '@modules/server.common/entities/Customer';
import Order                         from '@modules/server.common/entities/Order';
import { CommonUtils }               from '@modules/server.common/utilities';
import { env }                       from '../../env';
import { createLogger }              from '../../helpers/Log';

export interface IStripePaymentAddResponse
{
	cardId: string;
	customerId?: string;
}

export interface IStripeAddOptions
{
	customer: Customer;
	tokenId: string
}

export interface YooKassaAddOptions
{
	cardId: string;
}

export type MethodAddOptions = IStripeAddOptions & YooKassaAddOptions

function stripeFactory(secretKey: string, servicesEndpoint: string = env.SERVICES_ENDPOINT)
{
	const [host, port] = CommonUtils.getHostAndPort(servicesEndpoint);
	return new Stripe(secretKey, { host: host, port: port });
}

function yookassaFactory(shopId: string, secretKey: string)
{
	const debug = !env.isProd;
	return new YooCheckout({ shopId, secretKey, debug })
}

export class StripePayment
{
	private readonly stripe = stripeFactory(env.STRIPE_SECRET_KEY, env.SERVICES_ENDPOINT);
	
	protected readonly log: Logger = createLogger({ name: 'stripeService' });
	
	constructor() {}
	
	/**
	 * Get Stripe Cards for given customer
	 *
	 * @param {Customer} customer
	 * @returns {Promise<ICard[]>}
	 * @memberof UsersService
	 */
	@asyncListener()
	async getCards(customer: Customer): Promise<ICard[]>
	{
		if(customer)
		{
			if(customer.stripeCustomerId != null)
			{
				return (
						await this.stripe
						          .customers
						          .listSources(
								          customer.stripeCustomerId,
								          {
									          object: 'card'
								          }
						          )
				).data;
			}
			else
			{
				return [];
			}
		}
		else
		{
			throw new Error(`Customer with the id ${customer.id} doesn't exist`);
		}
	}
	
	/**
	 * Add Payment Method (Credit Card) for the customer.
	 * If method called first time for given customer, it creates Customer record in the Stripe API and
	 * updates stripeCustomerId in our DB
	 *
	 * @param {PaymentAddOptions} options
	 * @returns {Promise<string>}
	 * @memberof UsersService
	 */
	@asyncListener()
	async addPaymentMethod(options: MethodAddOptions): Promise<IStripePaymentAddResponse>
	{
		const callId = uuid();
		const { customer, tokenId } = options;
		let responseObj: IStripePaymentAddResponse = { cardId: '' };
		
		this.log.info(
				{ callId, userId: customer.id, tokenId },
				'.addPaymentMethod(userId, tokenId) called'
		);
		let card: ICard;
		
		try
		{
			if(customer)
			{
				if(customer.stripeCustomerId == null)
				{
					const stripeCustomer: customers.ICustomer =
							      await this.stripe
							                .customers
							                .create({
								                        email:       customer.email,
								                        description: 'Customer id: ' + customer.id,
								                        metadata:    {
									                        userId: customer.id
								                        }
							                        });
					
					responseObj.customerId = stripeCustomer.id;
					// customer = await this.update(customerId, {
					// 	stripeCustomerId: stripeCustomer.id
					// });
				}
				
				card = (
						await this.stripe
						          .customers
						          .createSource(
								          customer.stripeCustomerId as string,
								          {
									          source: tokenId
								          }
						          )
				) as ICard;
				
				responseObj.cardId = card.id;
			}
			else
			{
				throw new Error(`Customer with the id ${customer.id} doesn't exist`);
			}
		} catch(err)
		{
			this.log.error(
					{ callId, customerId: customer.id, tokenId, err },
					'.addPaymentMethod(customer, tokenId) thrown error!'
			);
			throw err;
		}
		
		this.log.info(
				{ callId, customerId: customer.id, tokenId, card },
				'.addPaymentMethod(customer, tokenId) added payment method'
		);
		
		return responseObj;
	}
	
	async pay(order: Order, cardId: string): Promise<Order>
	{
		const callId = uuid();
		const orderId = order.id;
		this.log.info(
				{ callId, orderId, cardId },
				'.pay(orderId, cardId) called'
		);
		
		try
		{
			if(order)
			{
				const customer = order.customer as Customer;
				
				if(customer)
				{
					const charge: Stripe.charges.ICharge = await this.stripe.charges.create(
							{
								amount:   order.totalPrice * 100, // amount in cents, again
								customer: customer.stripeCustomerId,
								source:   cardId,
								// @ts-ignore
								currency: order.orderCurrency.toLowerCase(),
								capture:  false, // holding amount
								// TODO: synchronize with warehouse configuration
								application_fee_amount: 10 * 100,
								description:            'Order id: ' + orderId,
								metadata:               {
									orderId
								}
							}
					);
					
					// order = await this.update(orderId, {
					// 	stripeChargeId: charge.id,
					// 	isPaid:         true
					// });
				}
				else
				{
					const msg = "Customer specified in order is not found!";
					this.log.error(
							{ callId, orderId, cardId, message: msg },
							'.pay(orderId, cardId) thrown error!'
					);
					throw new Error(msg);
				}
			}
			else
			{
				const msg = "Couldn't find order with such id";
				this.log.error(
						{ callId, orderId, cardId, message: msg },
						'.pay(orderId, cardId) thrown error!'
				);
				throw new Error(msg);
			}
		} catch(err)
		{
			this.log.error(
					{ callId, orderId, cardId, err },
					'.pay(orderId, cardId) thrown error!'
			);
			throw err;
		}
		
		this.log.info(
				{ callId, orderId, cardId, order },
				'.pay(orderId, cardId) accepted payment'
		);
		
		return order;
	}
}

/**
 * Payments Service
 *
 * @export
 * @class PaymentsService
 */
@injectable()
@routerName('payments')
class PaymentsService
{
	constructor(
			// @inject(YooKassaService)
			// private _yooPaymentService: YooKassaService
	)
	{}
}

@injectable()
@routerName('yookassa-payment')
class YooKassaService
{
	private yookassa = yookassaFactory(env.YOOMONEY_SHOP_ID, env.YOOMONEY_SECRET_KEY);
	
	public pay(order: Order)
	{
		try
		{
			const key = uuid();
			
			const orderItems = (order: Order) =>
			{
				let itemsArr: IItemWithoutData[] = [];
				
				for(let prod of order.products)
				{
					itemsArr.push({
						              description:     prod.comment,
						              quantity:        prod.count.toString(),
						              amount:          {
							              value:    prod.price.toString(),
							              currency: order.orderCurrency.toLowerCase() ?? 'rub'
						              },
						              vat_code:        0,
						              product_code:    prod.id,
						              payment_subject: "payment",
						              payment_mode:    "full_payment"
					              })
				}
				
				return itemsArr;
			}
			
			const cstmr: ICheckoutCustomer = {
				email:     order.customer.email,
				full_name: order.customer.fullName
			}
			
			const checkout: ICreatePayment = {
				amount:      {
					value:    order.totalPrice.toString(),
					currency: order.orderCurrency
				},
				description: order.orderNotes,
				receipt:     {
					customer: cstmr,
					items:    orderItems(order),
					phone:    order.customer.phone,
					email:    order.customer.email
				},
				capture:     ''
			}
			
			this.yookassa.createPayment(checkout);
		} catch(e)
		{
			console.error(e);
		}
	}
	
}

@injectable()
@routerName('stripe')
class PayPalService
{

}
