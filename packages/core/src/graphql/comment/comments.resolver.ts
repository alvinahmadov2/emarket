// import { UseGuards }                 from '@nestjs/common';
// import { AuthGuard }                 from '@nestjs/passport';
import { Mutation, Query, Resolver }                  from '@nestjs/graphql';
import Logger                                         from 'bunyan';
import { ICommentCreateObject, ICommentUpdateObject } from '@modules/server.common/interfaces/IComment';
import IPagingOptions                                 from '@modules/server.common/interfaces/IPagingOptions';
import Comment                                        from '@modules/server.common/entities/Comment';
import { CommentsService }                            from '../../services/comments';
import { createLogger }                               from '../../helpers/Log';

export type TCommentUpdateInput = {
	storeId: string,
	storeProductId: string,
	commentId: string,
	comment: ICommentUpdateObject
}

export type TCommentRateInput = {
	storeId: string;
	storeProductId: string;
	userId: string;
	commentId: string;
}

export type TCommentCreateInput = {
	storeId: string;
	storeProductId: string;
	comment: ICommentCreateObject
}

export type TCommentDeleteInput = {
	storeId: string;
	storeProductId: string;
	commentIds: string[];
}

export type TCommentCountInput = {
	storeId: string;
	storeProductId: string;
}

export type TCommentsRetrieveInput = {
	storeId: string;
	storeProductId: string;
	pagingOptions?: IPagingOptions
}

export type TCommentRetrieveInput = {
	storeId: string;
	storeProductId: string;
	commentId: string
}

export interface ICommentsResolver
{
	getComment(context: any, retrieveInput: TCommentRetrieveInput): Promise<Comment>;
	
	getComments(context: any, retrieveInput: TCommentsRetrieveInput): Promise<Comment[]>;
	
	getCountOfComments(context: any, countInput: TCommentCountInput): Promise<number>;
	
	addComment(context: any, createInput: TCommentCreateInput): Promise<Comment[]>;
	
	updateComment(context: any, updateInput: TCommentUpdateInput): Promise<Comment>;
	
	deleteCommentsByIds(context: any, deleteInput: TCommentDeleteInput): Promise<Comment[]>;
}

@Resolver('Comment')
export class CommentsResolver implements ICommentsResolver
{
	protected readonly logger: Logger = createLogger({
		                                                 name: 'commentsResolver',
		                                                 ns:   'graphql'
	                                                 });
	
	constructor(private readonly _commentsService: CommentsService) {}
	
	@Query('comment')
	public async getComment(_, retrieveInput: TCommentRetrieveInput): Promise<Comment>
	{
		this.logger.debug(".getComment(retrieveInput) called with", retrieveInput);
		return this._commentsService.get(
				retrieveInput.storeId,
				retrieveInput.storeProductId,
				retrieveInput.commentId
		).toPromise();
	}
	
	@Query('comments')
	public async getComments(_, retrieveInput: TCommentsRetrieveInput): Promise<Comment[]>
	{
		this.logger.debug(".getComments(retrieveInput) called with", retrieveInput);
		let pagingOptions = retrieveInput.pagingOptions ?? {};
		
		if(!pagingOptions || (pagingOptions && !pagingOptions.sort))
		{
			pagingOptions.sort = { field: '_createdAt', sortBy: 'desc' };
		}
		
		return this._commentsService.getComments(
				retrieveInput.storeId,
				retrieveInput.storeProductId,
				pagingOptions
		);
	}
	
	@Query()
	public async getCountOfComments(_, countInput: TCommentCountInput): Promise<number>
	{
		this.logger.debug(".getCountOfComments(countInput) called with", countInput);
		const comments = await this._commentsService.getComments(
				countInput.storeId,
				countInput.storeProductId
		);
		return comments.length;
	}
	
	@Mutation()
	public async addComment(_, createInput: TCommentCreateInput): Promise<Comment[]>
	{
		this.logger.debug(".addComment(createInput) called with", createInput);
		return await this._commentsService.add(
				createInput.storeId,
				createInput.storeProductId,
				createInput.comment
		);
	}
	
	@Mutation()
	// @UseGuards(AuthGuard('jwt'))
	public async updateComment(_, updateInput: TCommentUpdateInput): Promise<Comment>
	{
		this.logger.debug(".updateComment(updateInput) called with", updateInput);
		return this._commentsService.update(
				updateInput.storeId,
				updateInput.storeProductId,
				updateInput.commentId,
				updateInput.comment
		);
	}
	
	@Mutation('increaseCommentLikes')
	public async increaseLikes(_, rateInput: TCommentRateInput): Promise<Comment>
	{
		this.logger.debug(".addComment(commentInput) called with", rateInput);
		return await this._commentsService.increaseLikes(
				rateInput.storeId,
				rateInput.storeProductId,
				rateInput.userId,
				rateInput.commentId
		);
	}
	@Mutation('increaseCommentDislikes')
	public async increaseDislikes(_, rateInput: TCommentRateInput): Promise<Comment>
	{
		this.logger.debug(".increaseDislikes(rateInput) called with", rateInput);
		return await this._commentsService.increaseDislikes(
				rateInput.storeId,
				rateInput.storeProductId,
				rateInput.userId,
				rateInput.commentId
		);
	}
	
	@Mutation()
	// @UseGuards(AuthGuard('jwt'))
	public async deleteCommentsByIds(_, deleteInput: TCommentDeleteInput): Promise<Comment[]>
	{
		this.logger.debug(".deleteCommentsByIds(deleteInput) called with", deleteInput);
		return await this._commentsService.delete(
				deleteInput.storeId,
				deleteInput.storeProductId,
				deleteInput.commentIds
		);
	}
}
