// noinspection JSUnusedGlobalSymbols

/* tslint:disable */
/* eslint-disable */
export enum CarrierStatus
{
	Online  = "Online",
	Offline = "Offline",
	Blocked = "Blocked"
}

export enum Language
{
	he_IL = "he_IL",
	en_US = "en_US",
	ru_RU = "ru_RU",
	bg_BG = "bg_BG"
}

export enum OrderCarrierStatus
{
	NoCarrier                = "NoCarrier",
	CarrierSelectedOrder     = "CarrierSelectedOrder",
	CarrierPickedUpOrder     = "CarrierPickedUpOrder",
	CarrierStartDelivery     = "CarrierStartDelivery",
	CarrierArrivedToCustomer = "CarrierArrivedToCustomer",
	DeliveryCompleted        = "DeliveryCompleted",
	IssuesDuringDelivery     = "IssuesDuringDelivery",
	ClientRefuseTakingOrder  = "ClientRefuseTakingOrder"
}

export enum OrderWarehouseStatus
{
	NoStatus                   = "NoStatus",
	ReadyForProcessing         = "ReadyForProcessing",
	WarehouseStartedProcessing = "WarehouseStartedProcessing",
	AllocationStarted          = "AllocationStarted",
	AllocationFinished         = "AllocationFinished",
	PackagingStarted           = "PackagingStarted",
	PackagingFinished          = "PackagingFinished",
	GivenToCarrier             = "GivenToCarrier",
	AllocationFailed           = "AllocationFailed",
	PackagingFailed            = "PackagingFailed"
}

export interface AdditionalCustomerRegistrationInfo
{
	email: string;
	password: string;
	firstName?: string;
	lastName?: string;
	phone?: string;
}

export interface AdminCreateInput
{
	username: string;
	email: string;
	avatar: string;
	firstName?: string;
	lastName?: string;
}

export interface AdminFindInput
{
	username?: string;
	email?: string;
	firstName?: string;
	lastName?: string;
}

export interface AdminPasswordUpdateInput
{
	current: string;
	new: string;
}

export interface AdminRegisterInput
{
	admin: AdminCreateInput;
	password: string;
}

export interface AdminUpdateInput
{
	username?: string;
	email?: string;
	avatar?: string;
	firstName?: string;
	lastName?: string;
}

export interface CarrierCreateInput
{
	email?: string;
	firstName: string;
	lastName: string;
	geoLocation: GeoLocationCreateInput;
	status?: number;
	username: string;
	password: string;
	phone: string;
	logo: string;
	numberOfDeliveries?: number;
	skippedOrderIds?: string[];
	deliveriesCountToday?: number;
	totalDistanceToday?: number;
	isSharedCarrier?: boolean;
	devicesIds?: string[];
	isDeleted?: boolean;
}

export interface CarrierOrdersOptions
{
	populateWarehouse: boolean;
	completion: string;
}

export interface CarrierPasswordUpdateInput
{
	current: string;
	new: string;
}

export interface CarrierRegisterInput
{
	carrier: CarrierCreateInput;
	password: string;
}

export interface CarriersFindInput
{
	firstName?: string;
	lastName?: string;
	email?: string;
	phone?: string;
	isDeleted?: boolean;
	status?: number;
	isSharedCarrier?: boolean;
	_id?: Any;
}

export interface CarrierUpdateInput
{
	firstName?: string;
	lastName?: string;
	geoLocation?: GeoLocationUpdateInput;
	status?: number;
	username?: string;
	phone?: string;
	email?: string;
	logo?: string;
	numberOfDeliveries?: number;
	skippedOrderIds?: string[];
	deliveriesCountToday?: number;
	totalDistanceToday?: number;
	devicesIds?: string[];
	isSharedCarrier?: boolean;
	isActive?: boolean;
}

export interface ConversationCreateInput
{
	channelId: string;
	participants?: string[];
	platform?: string;
	locale?: string;
}

export interface ConversationFindInput
{
	channelId?: string;
	participants?: string[];
	platform?: string;
	locale?: string;
}

export interface CurrencyCreateInput
{
	code: string;
	name?: string;
	sign?: string;
	order?: string;
}

export interface CustomerCreateInput
{
	username: string;
	email: string;
	firstName?: string;
	lastName?: string;
	phone?: string;
	avatar?: string;
	geoLocation: GeoLocationCreateInput;
	apartment: string;
}

export interface CustomerFindInput
{
	id?: string;
	firstName?: string;
	lastName?: string;
	username?: string;
	email?: string;
	phone?: string;
	apartment?: string;
	avatar?: string;
}

export interface CustomerMemberInput
{
	exceptCustomerId?: string;
	memberKey: string;
	memberValue: string;
}

export interface CustomerPasswordUpdateInput
{
	current: string;
	new: string;
}

export interface CustomerRegisterInput
{
	user: CustomerCreateInput;
	password?: string;
}

export interface CustomerUpdateObjectInput
{
	geoLocation: GeoLocationUpdateObjectInput;
	devicesIds?: string[];
	apartment?: string;
	stripeCustomerId?: string;
}

export interface DeviceCreateInput
{
	channelId: string;
	language?: string;
	type: string;
	uuid: string;
}

export interface DeviceFindInput
{
	channelId?: string;
	language?: string;
	type?: string;
	uuid?: string;
}

export interface DeviceUpdateInput
{
	channelId?: string;
	language?: string;
	type?: string;
	uuid?: string;
}

export interface GeoLocationCreateInput
{
	countryId: number;
	city: string;
	streetAddress: string;
	house: string;
	postcode?: string;
	notes?: string;
	loc: LocInput;
}

export interface GeoLocationCreateObject
{
	loc: Location;
	countryId: number;
	city: string;
	postcode: string;
	streetAddress: string;
	house: string;
}

export interface GeoLocationFindInput
{
	countryId?: number;
	city?: string;
	streetAddress?: string;
	house?: string;
	postcode?: string;
	notes?: string;
	loc?: LocInput;
}

export interface GeoLocationInput
{
	countryId: number;
	countryName?: string;
	city: string;
	streetAddress: string;
	house: string;
	postcode?: string;
	notes?: string;
	loc: Location;
}

export interface GeoLocationOrdersOptions
{
	sort?: string;
	limit?: number;
	skip?: number;
}

export interface GeoLocationUpdateInput
{
	countryId?: number;
	city?: string;
	streetAddress?: string;
	house?: string;
	postcode?: string;
	notes?: string;
	loc?: LocInput;
}

export interface GeoLocationUpdateObjectInput
{
	loc: Location;
}

export interface GetGeoLocationProductsOptions
{
	isDeliveryRequired?: boolean;
	isTakeaway?: boolean;
	trackingDistance?: number;
	merchantIds?: string[];
	imageOrientation?: number;
	locale?: string;
	withoutCount?: boolean;
}

export interface GetGeoLocationWarehousesOptions
{
	activeOnly?: boolean;
	maxDistance?: number;
	inStoreMode?: boolean;
	fullProducts?: boolean;
}

export interface ImageInput
{
	locale: string;
	url: string;
	width: number;
	height: number;
	orientation: number;
}

export interface InviteByCodeInput
{
	location: Location;
	inviteCode: string;
	firstName?: string;
	lastName?: string;
}

export interface InviteByLocationInput
{
	countryId: number;
	city: string;
	streetAddress: string;
	house: string;
	apartment: string;
	postcode?: string;
	notes?: string;
}

export interface InviteCreateInput
{
	code?: string;
	apartment: string;
	geoLocation: GeoLocationCreateInput;
}

export interface InviteInput
{
	code: string;
	apartment: string;
	geoLocation: GeoLocationInput;
	isDeleted: boolean;
}

export interface InviteRequestCreateInput
{
	apartment: string;
	geoLocation: GeoLocationCreateInput;
	isManual?: boolean;
	invitedDate?: Date;
	isInvited?: boolean;
}

export interface InviteRequestUpdateInput
{
	apartment?: string;
	geoLocation?: GeoLocationUpdateInput;
	isManual?: boolean;
	invitedDate?: Date;
	isInvited?: boolean;
}

export interface InvitesFindInput
{
	code?: string;
	apartment?: string;
}

export interface InvitesRequestsFindInput
{
	apartment?: string;
	isManual?: boolean;
	isInvited?: boolean;
	invitedDate?: Date;
}

export interface InviteUpdateInput
{
	code?: string;
	apartment?: string;
	geoLocation?: GeoLocationUpdateInput;
}

export interface Location
{
	type: string;
	coordinates: number[];
}

export interface LocInput
{
	type: string;
	coordinates: number[];
}

export interface OrderCreateInput
{
	customerId: string;
	warehouseId: string;
	products: OrderProductCreateInput[];
	options?: WarehouseOrdersRouterCreateOptions;
	orderType?: number;
}

export interface OrderProductCreateInput
{
	count: number;
	productId: string;
	comment?: string;
}

export interface OrdersFindInput
{
	customer?: string;
	warehouse?: string;
	carrier?: string;
	isConfirmed?: boolean;
	isCancelled?: boolean;
	isPaid?: boolean;
	warehouseStatus: number;
	carrierStatus: number;
	orderNumber?: number;
}

export interface PagingOptionsInput
{
	sort?: PagingSortInput;
	limit?: number;
	skip?: number;
}

export interface PagingSortInput
{
	field: string;
	sortBy: string;
}

export interface ProductCreateInput
{
	title: TranslateInput[];
	description: TranslateInput[];
	details?: TranslateInput[];
	images: ImageInput[];
	categories?: ProductsCategoryInput[];
	detailsHTML?: TranslateInput[];
	descriptionHTML?: TranslateInput[];
}

export interface ProductSaveInput
{
	_id: string;
	id?: string;
	title: TranslateInput[];
	description: TranslateInput[];
	details?: TranslateInput[];
	images: ImageInput[];
	categories?: ProductsCategoryInput[];
	detailsHTML?: TranslateInput[];
	descriptionHTML?: TranslateInput[];
}

export interface ProductsCategoriesCreateInput
{
	name: TranslateInput[];
	image?: string;
}

export interface ProductsCategoriesFindInput
{
	noop?: Void;
}

export interface ProductsCategoriesUpdatenput
{
	name?: TranslateInput[];
}

export interface ProductsCategoryInput
{
	_id: string;
	name?: TranslateInput[];
}

export interface ProductsFindInput
{
	title?: TranslateInput;
	description?: TranslateInput;
	details?: TranslateInput;
	image?: ImageInput;
}

export interface PromotionInput
{
	title?: TranslateInput[];
	description?: TranslateInput[];
	promoPrice?: number;
	warehouse?: WarehouseInput;
	active?: boolean;
	activeFrom?: Date;
	activeTo?: Date;
	image?: string;
	product?: string;
	purchasesCount?: number;
}

export interface PromotionsFindInput
{
	warehouse?: string;
}

export interface QuantityUpdateInput
{
	change?: number;
	to?: number;
}

export interface SearchByRegex
{
	key: string;
	value: string;
}

export interface SearchOrdersForWork
{
	isCancelled?: boolean;
	byRegex?: SearchByRegex[];
}

export interface TranslateInput
{
	locale: string;
	value: string;
}

export interface WarehouseCreateInput
{
	geoLocation: GeoLocationCreateInput;
	name: string;
	logo: string;
	username: string;
	isActive?: boolean;
	isPaymentEnabled?: boolean;
	contactEmail?: string;
	contactPhone?: string;
	forwardOrdersUsing?: number[];
	ordersEmail?: string;
	ordersPhone?: string;
	isManufacturing?: boolean;
	isCarrierRequired?: boolean;
	hasRestrictedCarriers?: boolean;
	carriersIds?: string[];
	usedCarriersIds?: string[];
	useOnlyRestrictedCarriersForDelivery?: boolean;
	preferRestrictedCarriersForDelivery?: boolean;
	inStoreMode: boolean;
	carrierCompetition: boolean;
}

export interface WarehouseInput
{
	_id?: string;
}

export interface WarehouseOrdersRouterCreateOptions
{
	autoConfirm: boolean;
}

export interface WarehousePasswordUpdateInput
{
	current: string;
	new: string;
}

export interface WarehouseProductInput
{
	price: number;
	initialPrice: number;
	count?: number;
	product: string;
	isManufacturing?: boolean;
	isCarrierRequired?: boolean;
	isDeliveryRequired?: boolean;
	isProductAvailable?: boolean;
	isTakeaway?: boolean;
	deliveryTimeMin?: number;
	deliveryTimeMax?: number;
}

export interface WarehouseProductUpdateInput
{
	quantity?: QuantityUpdateInput;
	price?: number;
}

export interface WarehouseRegisterInput
{
	warehouse: WarehouseCreateInput;
	password: string;
}

export interface WarehousesFindInput
{
	isActive?: boolean;
	isPaymentEnabled?: boolean;
	name?: string;
	logo?: string;
	username?: string;
	contactEmail?: string;
	contactPhone?: string;
	forwardOrdersUsing?: number[];
	ordersEmail?: string;
	ordersPhone?: string;
	isManufacturing?: boolean;
	isCarrierRequired?: boolean;
	hasRestrictedCarriers?: boolean;
	carriersIds?: string[];
	usedCarriersIds?: string[];
	useOnlyRestrictedCarriersForDelivery?: boolean;
	preferRestrictedCarriersForDelivery?: boolean;
	inStoreMode: boolean;
	carrierCompetition: boolean;
}

export interface Admin
{
	_id: string;
	id: string;
	username: string;
	email: string;
	avatar: string;
	role?: string;
	firstName?: string;
	lastName?: string;
}

export interface AdminAppSettings
{
	adminPasswordReset: number;
	fakeDataGenerator: number;
}

export interface AdminLoginInfo
{
	admin: Admin;
	token: string;
}

export interface Carrier
{
	_id: string;
	id: string;
	firstName: string;
	lastName: string;
	username: string;
	phone: string;
	logo: string;
	email?: string;
	numberOfDeliveries: number;
	skippedOrderIds?: string[];
	status: number;
	geoLocation: GeoLocation;
	devicesIds: string[];
	apartment?: string;
	isActive?: boolean;
	isSharedCarrier?: boolean;
	devices: Device[];
	isDeleted: boolean;
}

export interface CarrierLoginInfo
{
	carrier: Carrier;
	token: string;
}

export interface CarrierOrder
{
	_id: string;
	id: string;
	isConfirmed: boolean;
	isCancelled: boolean;
	isPaid: boolean;
	warehouseStatus: number;
	carrierStatus: number;
	orderNumber: number;
	_createdAt?: Date;
	user: CarrierOrderUser;
	warehouse: CarrierOrderWarehouse;
	carrier: CarrierOrderCarrier;
	products: CarrierOrderProducts[];
}

export interface CarrierOrderCarrier
{
	id: string;
}

export interface CarrierOrderProducts
{
	count: number;
	isManufacturing: boolean;
	isCarrierRequired: boolean;
	isDeliveryRequired: boolean;
	isTakeaway?: boolean;
	initialPrice: number;
	price: number;
	product: CarrierOrderProductsProduct;
}

export interface CarrierOrderProductsProduct
{
	_id: string;
	id: string;
	title: TranslateType[];
	description: TranslateType[];
	details: TranslateType[];
	images: ImageType[];
	categories?: string[];
}

export interface CarrierOrderUser
{
	_id: string;
	firstName?: string;
	lastName?: string;
	geoLocation: GeoLocation;
}

export interface CarrierOrderWarehouse
{
	logo: string;
	name: string;
	geoLocation: GeoLocation;
}

export interface Category
{
	id?: string;
	name: TranslateType[];
}

export interface CompletedOrderInfo
{
	totalOrders: number;
	totalRevenue: number;
}

export interface Conversation
{
	_id: string;
	id: string;
	channelId: string;
	participants?: string[];
	platform?: string;
	locale?: string;
}

export interface Currency
{
	_id: string;
	code: string;
	name?: string;
	sign?: string;
	order?: string;
}

export interface Customer
{
	_id: string;
	id: string;
	username: string;
	email: string;
	geoLocation: GeoLocation;
	apartment: string;
	role?: string;
	firstName?: string;
	lastName?: string;
	phone?: string;
	devicesIds: string[];
	devices: Device[];
	avatar?: string;
	fullAddress?: string;
	fullName?: string;
	createdAt?: Date;
	isBanned: boolean;
}

export interface CustomerLoginInfo
{
	user: Customer;
	token: string;
}

export interface CustomerMetrics
{
	totalOrders?: number;
	canceledOrders?: number;
	completedOrdersTotalSum?: number;
}

export interface CustomersByStore
{
	storeId: string;
	customersCount: number;
}

export interface DashboardCompletedOrder
{
	warehouseId: string;
	totalPrice: number;
}

export interface Device
{
	_id: string;
	id: string;
	channelId?: string;
	type: string;
	uuid: string;
	language?: string;
}

export interface ExistingCustomersByStores
{
	total: number;
	perStore: CustomersByStore[];
}

export interface GenerateOrdersResponse
{
	error: boolean;
	message?: string;
}

export interface GeoLocation
{
	_id?: string;
	id?: string;
	_createdAt?: Date;
	createdAt?: Date;
	_updatedAt?: Date;
	updatedAt?: Date;
	countryId: number;
	countryName?: string;
	city: string;
	streetAddress: string;
	house: string;
	postcode?: string;
	notes?: string;
	loc: Loc;
	coordinates: GeoLocationCoordinates;
}

export interface GeoLocationCoordinates
{
	lng: number;
	lat: number;
}

export interface ImageType
{
	locale: string;
	url: string;
	width: number;
	height: number;
	orientation: number;
}

export interface Invite
{
	_id: string;
	id: string;
	code: string;
	apartment: string;
	geoLocation: GeoLocation;
}

export interface InviteRequest
{
	_id: string;
	id: string;
	apartment: string;
	geoLocation: GeoLocation;
	isManual?: boolean;
	isInvited?: boolean;
	invitedDate?: Date;
}

export interface Loc
{
	type: string;
	coordinates: number[];
}

export interface MerchantsOrders
{
	_id?: string;
	ordersCount?: number;
}

export interface IMutation
{
	registerAdmin(registerInput: AdminRegisterInput): Admin | Promise<Admin>;
	
	adminLogin(email: string, password: string): AdminLoginInfo | Promise<AdminLoginInfo>;
	
	updateAdmin(id: string, updateInput: AdminUpdateInput): Admin | Promise<Admin>;
	
	updateAdminPassword(id: string, password: AdminPasswordUpdateInput): Void | Promise<Void>;
	
	registerCarrier(registerInput: CarrierRegisterInput): Carrier | Promise<Carrier>;
	
	updateCarrierEmail(id: string, email: string): Carrier | Promise<Carrier>;
	
	updateCarrier(id: string, updateInput: CarrierUpdateInput): Carrier | Promise<Carrier>;
	
	removeCarrier(id: string): Void | Promise<Void>;
	
	carrierLogin(username: string, password: string): CarrierLoginInfo | Promise<CarrierLoginInfo>;
	
	updateCarrierPassword(id: string, password: CarrierPasswordUpdateInput): Void | Promise<Void>;
	
	removeCarriersByIds(ids: string[]): string | Promise<string>;
	
	updateCarrierStatus(id: string, status?: CarrierStatus): Carrier | Promise<Carrier>;
	
	createConversation(createInput: ConversationCreateInput): Conversation | Promise<Conversation>;
	
	removeConversation(channelId: string): Void | Promise<Void>;
	
	createCurrency(createInput: CurrencyCreateInput): MutationResponse | Promise<MutationResponse>;
	
	updateCustomer(id: string, updateObject: CustomerUpdateObjectInput): Customer | Promise<Customer>;
	
	updateCustomerEmail(id: string, email: string): Customer | Promise<Customer>;
	
	registerCustomer(registerInput: CustomerRegisterInput): Customer | Promise<Customer>;
	
	customerLogin(email: string, password: string): CustomerLoginInfo | Promise<CustomerLoginInfo>;
	
	removeCustomersByIds(ids: string[]): string | Promise<string>;
	
	updateCustomerPassword(id: string, password: CustomerPasswordUpdateInput): Void | Promise<Void>;
	
	addCustomerRegistrationInfo(id: string, registrationInfo: AdditionalCustomerRegistrationInfo): Void | Promise<Void>;
	
	banCustomer(id: string): Customer | Promise<Customer>;
	
	unbanCustomer(id: string): Customer | Promise<Customer>;
	
	createDevice(createInput: DeviceCreateInput): Device | Promise<Device>;
	
	updateDevice(id: string, updateInput: DeviceUpdateInput): Device | Promise<Device>;
	
	removeDevice(id: string): Void | Promise<Void>;
	
	removeDeviceByIds(ids: string[]): Remove | Promise<Remove>;
	
	updateDeviceLanguage(deviceId: string, language: Language): Device | Promise<Device>;
	
	createInviteRequest(createInput: InviteRequestCreateInput): InviteRequest | Promise<InviteRequest>;
	
	updateInviteRequest(id: string, updateInput: InviteRequestUpdateInput): InviteRequest | Promise<InviteRequest>;
	
	removeInviteRequest(id: string): Void | Promise<Void>;
	
	removeInvitesRequestsByIds(ids: string[]): Remove | Promise<Remove>;
	
	createInvite(createInput: InviteCreateInput): Invite | Promise<Invite>;
	
	updateInvite(id: string, updateInput: InviteUpdateInput): Invite | Promise<Invite>;
	
	removeInvite(id: string): Void | Promise<Void>;
	
	removeInvitesByIds(ids: string[]): Remove | Promise<Remove>;
	
	updateOrderCarrierStatus(orderId: string, status: OrderCarrierStatus): Order | Promise<Order>;
	
	updateOrderWarehouseStatus(orderId: string, status: OrderWarehouseStatus): Order | Promise<Order>;
	
	payOrderWithStripe(orderId: string, cardId: string): Order | Promise<Order>;
	
	createProductsCategory(createInput?: ProductsCategoriesCreateInput): ProductsCategory | Promise<ProductsCategory>;
	
	updateProductsCategory(id: string, updateInput: ProductsCategoriesCreateInput): ProductsCategory | Promise<ProductsCategory>;
	
	removeProductsCategoriesByIds(ids: string[]): Remove | Promise<Remove>;
	
	createProduct(product: ProductCreateInput): Product | Promise<Product>;
	
	saveProduct(product: ProductSaveInput): Product | Promise<Product>;
	
	removeProductsByIds(ids: string[]): Remove | Promise<Remove>;
	
	createPromotion(createInput?: PromotionInput): Promotion | Promise<Promotion>;
	
	updatePromotion(id?: string, updateInput?: PromotionInput): Promotion | Promise<Promotion>;
	
	removePromotion(id: string): Void | Promise<Void>;
	
	removePromotionsByIds(ids: string[]): Remove | Promise<Remove>;
	
	createOrder(createInput: OrderCreateInput): Order | Promise<Order>;
	
	removeWarehouseProducts(storeId: string, productsIds: string[]): boolean | Promise<boolean>;
	
	addWarehouseProducts(storeId: string, products: WarehouseProductInput[]): WarehouseProduct[] | Promise<WarehouseProduct[]>;
	
	updateWarehouseProduct(storeId: string, productId: string, updateInput: WarehouseProductUpdateInput): WarehouseProduct | Promise<WarehouseProduct>;
	
	registerWarehouse(registerInput: WarehouseRegisterInput): Warehouse | Promise<Warehouse>;
	
	warehouseLogin(username: string, password: string): WarehouseLoginInfo | Promise<WarehouseLoginInfo>;
	
	isAuthenticated(token: string): boolean | Promise<boolean>;
	
	removeWarehousesByIds(ids: string[]): Void | Promise<Void>;
	
	updateWarehousePassword(id: string, password: WarehousePasswordUpdateInput): Void | Promise<Void>;
	
	updateStoreGeoLocation(storeId: string, geoLocation: GeoLocationCreateObject): Warehouse | Promise<Warehouse>;
}

export interface MutationResponse
{
	success: boolean;
	message?: string;
	data?: Currency;
}

export interface Order
{
	_id: string;
	id: string;
	customer: Customer;
	warehouse: Warehouse;
	warehouseId: string;
	carrier?: Carrier;
	carrierId?: string;
	products: OrderProduct[];
	isConfirmed: boolean;
	isCancelled: boolean;
	waitForCompletion?: boolean;
	isPaid: boolean;
	isCompleted: boolean;
	totalPrice: number;
	orderType?: number;
	deliveryTime?: Date;
	finishedProcessingTime?: Date;
	startDeliveryTime?: Date;
	deliveryTimeEstimate?: number;
	warehouseStatus: number;
	carrierStatus: number;
	orderNumber: number;
	orderCurrency?: number;
	orderNotes?: string;
	carrierStatusText: string;
	warehouseStatusText: string;
	status?: number;
	createdAt?: Date;
	_createdAt?: Date;
	updatedAt?: Date;
	_updatedAt?: Date;
}

export interface OrderCancelation
{
	enabled?: boolean;
	onState?: number;
}

export interface OrderChartPanel
{
	isCancelled: boolean;
	isCompleted: boolean;
	totalPrice: number;
	_createdAt: Date;
}

export interface OrderCountTnfo
{
	id?: string;
	ordersCount?: number;
}

export interface OrderedUserInfo
{
	customer: Customer;
	ordersCount: number;
	totalPrice: number;
}

export interface OrderProduct
{
	_id: string;
	count: number;
	isManufacturing: boolean;
	isCarrierRequired: boolean;
	isDeliveryRequired: boolean;
	isTakeaway?: boolean;
	initialPrice: number;
	price: number;
	product: Product;
	comment?: string;
}

export interface Product
{
	_id: string;
	id: string;
	title: TranslateType[];
	description: TranslateType[];
	details: TranslateType[];
	images: ImageType[];
	categories?: string[];
	detailsHTML: TranslateType[];
	descriptionHTML: TranslateType[];
	_createdAt?: Date;
	_updatedAt?: Date;
}

export interface ProductComment
{
	comment: string;
	likes?: number;
	dislikes?: number;
	isReply?: boolean;
	replyTo?: string;
}

export interface ProductInfo
{
	warehouseProduct: WarehouseProduct;
	distance: number;
	warehouseId: string;
	warehouseLogo: string;
}

export interface ProductObject
{
	_id: string;
}

export interface ProductsCategory
{
	_id: string;
	id: string;
	name: TranslateType[];
	image?: string;
	_createdAt?: Date;
	_updatedAt?: Date;
}

export interface Promotion
{
	_id?: string;
	title?: TranslateType[];
	description?: TranslateType[];
	promoPrice?: number;
	warehouse?: Warehouse;
	product?: ProductObject;
	warehouseId?: string;
	productId?: string;
	active?: boolean;
	activeFrom?: Date;
	activeTo?: Date;
	image?: string;
	purchasesCount?: number;
}

export interface IQuery
{
	adminByEmail(email: string): Admin | Promise<Admin>;
	
	admin(id: string): Admin | Promise<Admin>;
	
	adminSearch(findInput?: AdminFindInput): Admin | Promise<Admin>;
	
	adminAuthenticated(): boolean | Promise<boolean>;
	
	adminAppSettings(): AdminAppSettings | Promise<AdminAppSettings>;
	
	getCarrierOrders(carrierId: string, options?: CarrierOrdersOptions): CarrierOrder[] | Promise<CarrierOrder[]>;
	
	getCarrierCurrentOrder(carrierId: string): Order | Promise<Order>;
	
	getCarrierOrdersHistory(carrierId: string, options?: GeoLocationOrdersOptions): Order[] | Promise<Order[]>;
	
	getCountOfCarrierOrdersHistory(carrierId: string): number | Promise<number>;
	
	getCarrierByUsername(username: string): Carrier | Promise<Carrier>;
	
	getCarrier(id: string): Carrier | Promise<Carrier>;
	
	getCarriers(carriersFindInput?: CarriersFindInput, pagingOptions?: PagingOptionsInput): Carrier[] | Promise<Carrier[]>;
	
	getActiveCarriers(): Carrier[] | Promise<Carrier[]>;
	
	getCountOfCarriers(carriersFindInput?: CarriersFindInput): number | Promise<number>;
	
	conversation(id?: string): Conversation | Promise<Conversation>;
	
	conversations(findInput?: ConversationFindInput): Conversation[] | Promise<Conversation[]>;
	
	currencies(): Currency[] | Promise<Currency[]>;
	
	customer(id: string): Customer | Promise<Customer>;
	
	customers(pagingOptions?: PagingOptionsInput): Customer[] | Promise<Customer[]>;
	
	findCustomers(findInput?: CustomerFindInput, pagingOptions?: PagingOptionsInput): Customer[] | Promise<Customer[]>;
	
	getOrders(customerId: string): Order[] | Promise<Order[]>;
	
	isCustomerExists(conditions: CustomerMemberInput): boolean | Promise<boolean>;
	
	getSocial(socialId: string): Customer | Promise<Customer>;
	
	isCustomerEmailExists(email: string): boolean | Promise<boolean>;
	
	generateCustomers(qty: number, defaultLng: number, defaultLat: number): ResponseGenerateCustomers | Promise<ResponseGenerateCustomers>;
	
	getCountOfCustomers(): number | Promise<number>;
	
	getCustomerMetrics(id: string): CustomerMetrics | Promise<CustomerMetrics>;
	
	clearAll(): boolean | Promise<boolean>;
	
	device(id?: string): Device | Promise<Device>;
	
	devices(findInput?: DeviceFindInput): Device[] | Promise<Device[]>;
	
	geoLocationProducts(geoLocation: GeoLocationFindInput): ProductInfo[] | Promise<ProductInfo[]>;
	
	geoLocationProductsByPaging(
			geoLocation: GeoLocationFindInput, pagingOptions?: PagingOptionsInput, options?: GetGeoLocationProductsOptions,
			searchText?: string
	): ProductInfo[] | Promise<ProductInfo[]>;
	
	getCountOfGeoLocationProducts(geoLocation: GeoLocationFindInput, options?: GetGeoLocationProductsOptions, searchText?: string): number | Promise<number>;
	
	getCloseMerchants(geoLocation: GeoLocationFindInput): Warehouse[] | Promise<Warehouse[]>;
	
	getNearMerchants(geoLocation?: GeoLocationFindInput, options?: GetGeoLocationWarehousesOptions): Warehouse[] | Promise<Warehouse[]>;
	
	getOrderForWork(geoLocation: GeoLocationFindInput, skippedOrderIds: string[], options?: GeoLocationOrdersOptions, searchObj?: SearchOrdersForWork): Order | Promise<Order>;
	
	getOrdersForWork(geoLocation: GeoLocationFindInput, skippedOrderIds: string[], options?: GeoLocationOrdersOptions, searchObj?: SearchOrdersForWork): Order[] | Promise<Order[]>;
	
	getCountOfOrdersForWork(geoLocation: GeoLocationFindInput, skippedOrderIds: string[], searchObj?: SearchOrdersForWork): number | Promise<number>;
	
	inviteRequest(id: string): InviteRequest | Promise<InviteRequest>;
	
	invitesRequests(findInput?: InvitesRequestsFindInput, pagingOptions?: PagingOptionsInput, invited?: boolean): InviteRequest[] | Promise<InviteRequest[]>;
	
	notifyAboutLaunch(devicesIds: string[], invite?: InviteInput): Void | Promise<Void>;
	
	generate1000InviteRequests(defaultLng: number, defaultLat: number): Void | Promise<Void>;
	
	getCountOfInvitesRequests(invited?: boolean): number | Promise<number>;
	
	invite(id: string): Invite | Promise<Invite>;
	
	invites(findInput?: InvitesFindInput, pagingOptions?: PagingOptionsInput): Invite[] | Promise<Invite[]>;
	
	getInviteByCode(info: InviteByCodeInput): Invite | Promise<Invite>;
	
	getInviteByLocation(info?: InviteByLocationInput): Invite | Promise<Invite>;
	
	generate1000InvitesConnectedToInviteRequests(defaultLng: number, defaultLat: number): Void | Promise<Void>;
	
	getCountOfInvites(): number | Promise<number>;
	
	getOrder(id: string): Order | Promise<Order>;
	
	orders(findInput?: OrdersFindInput): Order[] | Promise<Order[]>;
	
	getDashboardCompletedOrders(): DashboardCompletedOrder[] | Promise<DashboardCompletedOrder[]>;
	
	getDashboardCompletedOrdersToday(): Order[] | Promise<Order[]>;
	
	getOrdersChartTotalOrders(): OrderChartPanel[] | Promise<OrderChartPanel[]>;
	
	getCompletedOrdersInfo(storeId?: string): CompletedOrderInfo | Promise<CompletedOrderInfo>;
	
	getOrderedUsersInfo(storeId: string): OrderedUserInfo[] | Promise<OrderedUserInfo[]>;
	
	generateOrdersByCustomerId(numberOfOrders: number, customerId: string): Void | Promise<Void>;
	
	addTakenOrders(carrierIds: string[]): Void | Promise<Void>;
	
	addOrdersToTake(): Void | Promise<Void>;
	
	generateActiveAndAvailableOrdersPerCarrier(): Void | Promise<Void>;
	
	generatePastOrdersPerCarrier(): Void | Promise<Void>;
	
	getUsersOrdersCountInfo(usersIds?: string[]): OrderCountTnfo[] | Promise<OrderCountTnfo[]>;
	
	getMerchantsOrdersCountInfo(merchantsIds?: string[]): OrderCountTnfo[] | Promise<OrderCountTnfo[]>;
	
	generateRandomOrdersCurrentStore(storeId: string, storeCreatedAt: Date, ordersLimit: number): GenerateOrdersResponse | Promise<GenerateOrdersResponse>;
	
	productsCategory(id: string): ProductsCategory | Promise<ProductsCategory>;
	
	productsCategories(findInput?: ProductsCategoriesFindInput): ProductsCategory[] | Promise<ProductsCategory[]>;
	
	product(id: string): Product | Promise<Product>;
	
	products(findInput?: ProductsFindInput, pagingOptions?: PagingOptionsInput, existedProductsIds?: string[]): Product[] | Promise<Product[]>;
	
	getCountOfProducts(existedProductsIds?: string[]): number | Promise<number>;
	
	promotions(findInput?: PromotionsFindInput): Promotion[] | Promise<Promotion[]>;
	
	getStoreCarriers(storeId: string): Carrier[] | Promise<Carrier[]>;
	
	getStoreOrders(storeId: string): Order[] | Promise<Order[]>;
	
	getNextOrderNumber(storeId: string): number | Promise<number>;
	
	getDashboardOrdersChartOrders(storeId: string): Order[] | Promise<Order[]>;
	
	getMerchantsOrders(): MerchantsOrders[] | Promise<MerchantsOrders[]>;
	
	getStoreOrdersTableData(storeId: string, pagingOptions?: PagingOptionsInput, status?: string): StoreOrdersTableData | Promise<StoreOrdersTableData>;
	
	getCountOfStoreOrders(storeId: string, status: string): number | Promise<number>;
	
	getOrdersInDelivery(storeId: string): Order[] | Promise<Order[]>;
	
	removeCustomerOrders(storeId: string, customerId: string): RemovedUserOrdersObj | Promise<RemovedUserOrdersObj>;
	
	getWarehouseProduct(storeId: string, productId: string): WarehouseProduct | Promise<WarehouseProduct>;
	
	getWarehouseProductInfo(storeId: string): ProductInfo[] | Promise<ProductInfo[]>;
	
	getWarehouseProductsWithPagination(storeId: string, pagingOptions?: PagingOptionsInput): WarehouseProduct[] | Promise<WarehouseProduct[]>;
	
	getWarehouseProductsCount(id: string): number | Promise<number>;
	
	warehouse(id: string): Warehouse | Promise<Warehouse>;
	
	warehouses(findInput?: WarehousesFindInput, pagingOptions?: PagingOptionsInput): Warehouse[] | Promise<Warehouse[]>;
	
	nearbyStores(geoLocation: GeoLocationFindInput): Warehouse[] | Promise<Warehouse[]>;
	
	getAllActiveStores(fullProducts: boolean): Warehouse[] | Promise<Warehouse[]>;
	
	getStoreCustomers(storeId: string): Customer[] | Promise<Customer[]>;
	
	getStoreProducts(storeId: string, fullProducts: boolean): WarehouseProduct[] | Promise<WarehouseProduct[]>;
	
	getStoreAvailableProducts(storeId: string): WarehouseProduct[] | Promise<WarehouseProduct[]>;
	
	countCustomers(storeId: string): number | Promise<number>;
	
	getCountExistingCustomers(): ExistingCustomersByStores | Promise<ExistingCustomersByStores>;
	
	getCountExistingCustomersToday(): ExistingCustomersByStores | Promise<ExistingCustomersByStores>;
	
	hasExistingStores(): boolean | Promise<boolean>;
	
	getCountOfMerchants(): number | Promise<number>;
	
	getAllStores(): Warehouse[] | Promise<Warehouse[]>;
	
	getMerchantsByName(searchName: string, geoLocation?: GeoLocationFindInput): Warehouse[] | Promise<Warehouse[]>;
}

export interface Remove
{
	n?: number;
	ok?: number;
}

export interface RemovedUserOrdersObj
{
	num?: number;
	modified?: number;
}

export interface ResponseGenerateCustomers
{
	success: boolean;
	message?: string;
}

export interface StoreOrdersTableData
{
	orders: Order[];
	page: number;
}

export interface ISubscription
{
	deviceCreated(): Device | Promise<Device>;
}

export interface TranslateType
{
	locale: string;
	value: string;
}

export interface Warehouse
{
	_id: string;
	id: string;
	_createdAt: Date;
	isActive: boolean;
	isPaymentEnabled?: boolean;
	geoLocation: GeoLocation;
	name: string;
	logo: string;
	username: string;
	contactEmail?: string;
	contactPhone?: string;
	forwardOrdersUsing?: number[];
	ordersEmail?: string;
	ordersPhone?: string;
	isManufacturing?: boolean;
	isCarrierRequired?: boolean;
	devicesIds: string[];
	devices: Device[];
	orders: Order[];
	customers: Customer[];
	hasRestrictedCarriers?: boolean;
	carriersIds?: string[];
	usedCarriersIds?: string[];
	carriers?: Carrier[];
	orderBarcodeType?: number;
	useOnlyRestrictedCarriersForDelivery?: boolean;
	preferRestrictedCarriersForDelivery?: boolean;
	ordersShortProcess?: boolean;
	orderCancelation: OrderCancelation;
	inStoreMode: boolean;
	carrierCompetition: boolean;
}

export interface WarehouseLoginInfo
{
	warehouse: Warehouse;
	token: string;
}

export interface WarehouseProduct
{
	id: string;
	_id: string;
	price: number;
	initialPrice: number;
	count?: number;
	soldCount: number;
	viewsCount?: number;
	likesCount?: number;
	product: Product;
	comments?: ProductComment[];
	isManufacturing?: boolean;
	isCarrierRequired?: boolean;
	isDeliveryRequired?: boolean;
	isProductAvailable?: boolean;
	isTakeaway?: boolean;
	deliveryTimeMin?: number;
	deliveryTimeMax?: number;
}

export type Any = any;
export type Void = any;
