export interface ITranslatable
{
	getTranslate(key: string);
}
