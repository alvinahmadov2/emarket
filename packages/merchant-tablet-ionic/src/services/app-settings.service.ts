import { Injectable }     from '@angular/core';
import { Apollo }         from 'apollo-angular';
import { Observable, of } from 'rxjs';
import { first, map }     from 'rxjs/operators';
import ApolloService      from '@modules/client.common.angular2/services/apollo.service';
import { GQLQuery }       from 'graphql/definitions';

@Injectable()
export class AppSettingsService extends ApolloService
{
	constructor(apollo: Apollo)
	{
		super(apollo, {
			serviceName: `Merchant::AppSettingsService`,
			debug:       true
		})
	}
	
	public getTermsOfUse(locale: string): Observable<string>
	{
		try
		{
			return this.apollo
			           .query<{
				           res: string
			           }>({
				              query:     GQLQuery.AppSettings.TermsOfUse,
				              variables: { locale }
			              })
			           .pipe(
					           first(),
					           map((result) => this.get(result))
			           )
		} catch(e)
		{
			console.error(e)
		}
		
		return of("");
	}
	
	public getAboutUs(locale: string): Observable<string>
	{
		try
		{
			return this.apollo
			           .query<{
				           res: string
			           }>({
				              query:     GQLQuery.AppSettings.AboutUs,
				              variables: { locale }
			              })
			           .pipe(
					           first(),
					           map((result) => this.get(result))
			           )
		} catch(e)
		{
			console.error(e)
		}
		
		return of("");
	}
	
	public getHelp(locale: string): Observable<string>
	{
		try
		{
			return this.apollo
			           .query<{
				           res: string
			           }>({
				              query:     GQLQuery.AppSettings.Help,
				              variables: { locale }
			              })
			           .pipe(
					           first(),
					           map((result) => this.get(result))
			           )
		} catch(e)
		{
			console.error(e)
		}
		
		return of("");
	}
	
	public getPrivacy(locale: string): Observable<string>
	{
		try
		{
			return this.apollo
			           .query<{
				           res: string
			           }>({
				              query:     GQLQuery.AppSettings.Privacy,
				              variables: { locale }
			              })
			           .pipe(
					           first(),
					           map((result) => this.get(result))
			           );
			
		} catch(e)
		{
			console.error(e)
		}
		
		return of("");
	}
}
