import { Component, OnDestroy } from '@angular/core';
import { Subscription }         from 'rxjs';
import { CustomerRouter }       from '@modules/client.common.angular2/routers/customer-router.service';
import { environment }          from 'environment';
import { AppSettingsService }   from 'services/app-settings.service';
import { StorageService }       from 'services/storage.service';

@Component({
	           selector:    'page-about',
	           templateUrl: 'about.html',
           })
export class AboutPage implements OnDestroy
{
	public useAboutHtml: string = '<h1>Loading...</h1>';
	public selectedLanguage: string;
	private sub: Subscription = new Subscription();
	public deviceId: string;
	public userId: string;
	public appVersion: string;
	
	constructor(
			private userRouter: CustomerRouter,
			private appSettingsService: AppSettingsService,
			private storageService: StorageService
	)
	{
		this.selectedLanguage = this.storageService.locale || 'en-US';
		this.deviceId = this.storageService.deviceId;
		this.userId = this.storageService.merchantId;
		this.appVersion = environment.APP_VERSION;
	}
	
	ngOnInit()
	{
		try
		{
			console.debug(this.selectedLanguage)
			if(this.deviceId)
			{
				this.sub = this.userRouter
				               .getAboutUs(this.userId, this.deviceId, this.selectedLanguage)
				               .subscribe((html) => this.useAboutHtml = html);
			}
			else
			{
				this.sub = this.appSettingsService.getAboutUs(this.selectedLanguage)
				               .subscribe((html) => this.useAboutHtml = html);
			}
		} catch(e)
		{
			console.error(e);
		}
	}
	
	ngOnDestroy()
	{
		this.sub?.unsubscribe();
	}
}
