interface IPayment
{
	makePayment(...args: any[]): void;
	
	makeRefund(...args: any[]): void;
}

export default IPayment;
