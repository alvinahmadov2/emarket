import { YooCheckout, ICreatePayment } from '@a2seven/yoo-checkout';
import IPayment                        from './IPayment';
import uuid                            from 'uuid';

export class YooKassa implements IPayment
{
	private readonly checkout: YooCheckout
	
	constructor(
			private readonly shopId: string,
			private readonly secretKey: string
	)
	{
		this.checkout = new YooCheckout(
				{
					shopId:    this.shopId,
					secretKey: this.secretKey
				}
		);
	}
	
	public async makePayment(payload: ICreatePayment)
	{
		const idempotenceKey = uuid();
		try
		{
			const payment = await this.checkout.createPayment(payload, idempotenceKey);
			console.log(payment)
		} catch(error)
		{
			console.error(error);
		}
	}
	
	public makeRefund(...args: any[]): void
	{
	}
}
