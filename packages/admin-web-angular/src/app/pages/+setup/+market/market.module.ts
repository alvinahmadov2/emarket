import { NgModule }                                       from '@angular/core';
import { CommonModule }                                   from '@angular/common';
import { MarketComponent }                                from './market.component';
import { ThemeModule }                                    from '@app/@theme';
import { RouterModule, Route }                            from '@angular/router';
import { ToasterModule }                                  from 'angular2-toaster';
import { NbButtonModule, NbInputModule, NbStepperModule } from '@nebular/theme';
import { TranslateModule }                                from '@ngx-translate/core';
import { FileUploaderComponent }                          from '@app/@shared/file-uploader/file-uploader.component';
import { MarketSetupComponentsModule }                    from './components/components.module';
import { MarketSetupPaymentsModule }                      from './components/payments/payments.module';

const routes: Route[] = [
	{
		path:      '',
		component: MarketComponent
	}
]

@NgModule({
	          declarations: [
		          MarketComponent
	          ],
	          imports:      [
		          CommonModule,
		          ThemeModule,
		          RouterModule.forChild(routes),
		          TranslateModule.forChild(),
		          ToasterModule.forRoot(),
		          ToasterModule,
		          NbStepperModule,
		          NbInputModule,
		          NbButtonModule,
		          MarketSetupComponentsModule,
		          MarketSetupPaymentsModule,
	          ]
          })
export class MarketModule {}
