import { Component, OnInit, ViewChild } from '@angular/core';
import { NbStepperComponent }           from '@nebular/theme';

import Warehouse                           from '@modules/server.common/entities/Warehouse';
import { MarketSetupBasicInfoComponent }   from './components/basic-info/basic-info.component';
import { MarketSetupContactInfoComponent } from './components/contact-info/contact-info.component';
import { MarketSetupPaymentsComponent }    from './components/payments/payments.component';
import { WarehouseAuthRouter }             from "@modules/client.common.angular2/routers/warehouse-auth-router.service";
import { ToasterService }                  from "angular2-toaster";

@Component({
	           selector:    'ea-market',
	           styleUrls:   ['./market.component.scss'],
	           templateUrl: './market.component.html'
           })
export class MarketComponent implements OnInit
{
	
	@ViewChild('nbStepper')
	nbStepper: NbStepperComponent;
	
	@ViewChild('basicInfo', { static: true })
	stepBasicInfo: MarketSetupBasicInfoComponent;
	@ViewChild('contactInfo', { static: true })
	stepContactInfo: MarketSetupContactInfoComponent;
	@ViewChild('payments')
	stepPayments: MarketSetupPaymentsComponent;
	
	currentStore: Warehouse;
	
	constructor(
			private warehouseAuthRouter: WarehouseAuthRouter,
			private readonly toasterService: ToasterService
	)
	{ }
	
	ngOnInit(): void
	{
	}
	
	get isOk()
	{
		return (
				this.stepBasicInfo.formValid &&
				this.stepContactInfo.contactInfoForm.valid
		);
	}
	
}
