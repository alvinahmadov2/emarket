import { NgModule }                                     from '@angular/core';
import { CommonModule }                                 from '@angular/common';
import { ThemeModule }                                  from '@app/@theme';
import { TranslateModule }                              from '@ngx-translate/core';
import { FormsModule }                                  from '@angular/forms';
import { NbRadioModule, NbButtonModule, NbInputModule } from '@nebular/theme';
import { TabModule }                                    from '@modules/client.common.angular2/components/tabs/tab.module';
import { FileUploaderModule }                           from '@app/@shared/file-uploader/file-uploader.module';
import { NotifyService }                                from '@app/@core/services/notify/notify.service';
import { MarketSetupInstructionsComponent }             from './instructions/instructions.component';
import { MarketSetupBasicInfoComponent }                from './basic-info/basic-info.component';
import { MarketSetupContactInfoComponent }              from './contact-info/contact-info.component';

const COMPONENTS = [
	MarketSetupInstructionsComponent,
	MarketSetupBasicInfoComponent,
	MarketSetupContactInfoComponent
];

@NgModule({
	          imports:      [
		          CommonModule,
		          ThemeModule,
		          FormsModule,
		          NbRadioModule,
		          TranslateModule.forChild(),
		          FileUploaderModule,
		          TabModule,
		          NbButtonModule,
		          NbInputModule,
	          ],
	          declarations: COMPONENTS,
	          exports:      COMPONENTS,
	          providers:    [NotifyService],
          })
export class MarketSetupComponentsModule {}
