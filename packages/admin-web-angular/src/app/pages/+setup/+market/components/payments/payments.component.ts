import {
	Component,
	EventEmitter,
	Output,
	Input,
	ViewChild,
	OnInit,
}                                   from '@angular/core';
import { LocationFormComponent }    from '@app/@shared/forms/location';
import IPaymentGatewayCreateObject  from '@modules/server.common/interfaces/IPaymentGateway';
import { PaymentGatewaysComponent } from '@app/@shared/payment-gateways/payment-gateways.component';

@Component({
	           selector:    'ea-market-setup-payments',
	           styleUrls:   ['./payments.component.scss'],
	           templateUrl: './payments.component.html',
           })
export class MarketSetupPaymentsComponent implements OnInit
{
	@ViewChild('paymentGateways', { static: true })
	public paymentGateways: PaymentGatewaysComponent;
	@Output()
	public previousStep: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	public nextStep: EventEmitter<boolean> = new EventEmitter<boolean>();
	
	@Input()
	public warehouseLogo: string;
	@Input()
	public locationForm: LocationFormComponent;
	
	public isPaymentEnabled = false;
	public isCashPaymentEnabled = true;
	
	public get isPaymentValid()
	{
		return !this.isPaymentEnabled || this.paymentGateways.isValid;
	}
	
	public get paymentsGateways(): IPaymentGatewayCreateObject[]
	{
		return this.paymentGateways.paymentsGateways;
	}
	
	public ngOnInit()
	{
		console.warn(this.isCashPaymentEnabled);
	}
	
	public isCashAllowed(ev)
	{
		this.isCashPaymentEnabled = ev;
	}
	
	public isOnlinePaymentAllowed(ev)
	{
		this.isPaymentEnabled = ev;
	}
}
