import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm }                                     from '@angular/forms';

@Component({
	           selector:    'ea-market-setup-contact-info',
	           styleUrls:   ['./contact-info.component.scss'],
	           templateUrl: './contact-info.component.html',
           })
export class MarketSetupContactInfoComponent
{
	@ViewChild('contactInfoForm', { static: true })
	contactInfoForm: NgForm;
	
	@Output()
	previousStep: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	nextStep: EventEmitter<boolean> = new EventEmitter<boolean>();
	
	forwardingEmail: boolean;
	forwardingPhone: boolean;
	contactInfoModel = {
		contactPhone:       '',
		forwardOrdersUsing: [],
		ordersEmail:        '',
		ordersPhone:        '',
	};
}
