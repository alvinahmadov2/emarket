// noinspection DuplicatedCode

import { Component, Input, ViewChild } from '@angular/core';
import PaymentGateways, {
	paymentGatewaysToString,
	paymentGatewaysLogo,
}                                      from '@modules/server.common/enums/PaymentGateways';
import Country                         from '@modules/server.common/enums/Country';
import { NgForm }                      from '@angular/forms';
import IPaymentGatewayCreateObject     from '@modules/server.common/interfaces/IPaymentGateway';
import Currency                        from '@modules/server.common/entities/Currency';

@Component({
	           selector:    'ea-bitpay-gateway',
	           templateUrl: './bitpay-gateway.component.html',
           })
export class BitpayGatewayComponent
{
	@ViewChild('bitpayConfigForm', { static: true })
	public bitpayConfigForm: NgForm;
	
	isBitpayEnabled: boolean;
	name = paymentGatewaysToString(PaymentGateways.Bitpay);
	logo = paymentGatewaysLogo(PaymentGateways.Bitpay);
	
	@Input()
	public currencies: Currency[] = [];
	@Input()
	public warehouseCountry: Country;
	
	// TODO: Check
	public configModel = {
		currency:       '',
		mode:           '',
		publishableKey: '',
		secretKey:      '',
		description:    '',
	};
	
	public bitpayTypes = ['sandbox', 'live'];
	
	public get isFormValid(): boolean
	{
		let isValid = false;
		
		if(this.bitpayConfigForm)
		{
			isValid =
					(this.bitpayConfigForm.touched ||
					 this.bitpayConfigForm.dirty) &&
					this.bitpayConfigForm.valid;
		}
		
		return isValid;
	}
	
	public get createObject(): IPaymentGatewayCreateObject | null
	{
		if(!this.isFormValid || !this.isBitpayEnabled)
		{
			return null;
		}
		
		return {
			paymentGateway:  PaymentGateways.Bitpay,
			configureObject: this.configModel,
		};
	}
	
	public setValue(data)
	{
		this.isBitpayEnabled = true;
		this.configModel.currency = data['bitpayCurrency'] || '';
		this.configModel.mode = data['bitpayMode'] || '';
		this.configModel.publishableKey = data['bitpayPublishableKey'] || '';
		this.configModel.secretKey = data['bitpaySecretKey'] || '';
		this.configModel.description = data['bitpayDescription'] || '';
	}
}
