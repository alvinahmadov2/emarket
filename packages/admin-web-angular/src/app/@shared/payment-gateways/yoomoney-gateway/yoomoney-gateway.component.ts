import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { NgForm }                                 from '@angular/forms';
import { TranslateService }                       from '@ngx-translate/core';
import { Subject }                                from 'rxjs';
import { takeUntil }                              from 'rxjs/operators';
import Country                                    from '@modules/server.common/enums/Country';
import PaymentGateways, {
	paymentGatewaysToString,
	paymentGatewaysLogo,
}                                                 from '@modules/server.common/enums/PaymentGateways';
import IPaymentGatewayCreateObject                from '@modules/server.common/interfaces/IPaymentGateway';
import Currency                                   from '@modules/server.common/entities/Currency';

@Component({
	           selector:    'ea-yooMoney-gateway',
	           templateUrl: './yoomoney-gateway.component.html',
           })
export class YooMoneyGatewayComponent
{
	@ViewChild('yooConfigForm', { static: true })
	public yooConfigForm: NgForm;
	
	public isYooEnabled: boolean;
	public name: string = paymentGatewaysToString(PaymentGateways.YooMoney);
	public logo = paymentGatewaysLogo(PaymentGateways.YooMoney);
	public invalidUrl: boolean;
	
	@Input()
	public currencies: Currency[] = [];
	@Input()
	public warehouseCountry: Country;
	public configModel = {
		payButtontext:    '',
		currency:         '',
		publishableKey:   '',
		shopId:           '',
		secretKey:        '',
	};
	private _ngDestroy$ = new Subject<void>();
	
	constructor(private translateService: TranslateService)
	{
	}
	
	get isFormValid(): boolean
	{
		let isValid = false;
		
		if(this.yooConfigForm)
		{
			isValid =
					(this.yooConfigForm.touched ||
					 this.yooConfigForm.dirty) &&
					this.yooConfigForm.valid &&
					!this.invalidUrl;
		}
		
		return isValid;
	}
	
	get createObject(): IPaymentGatewayCreateObject | null
	{
		if(!this.isFormValid || !this.isYooEnabled)
		{
			return null;
		}
		
		return {
			paymentGateway:  PaymentGateways.YooMoney,
			configureObject: this.configModel,
		};
	}
	
	public setValue(data)
	{
		this.isYooEnabled = true;
		this.configModel.payButtontext = data['yooPayButtontext'] || '';
		this.configModel.currency = data['yooCurrency'] || '';
		this.configModel.publishableKey = data['yooPublishableKey'] || '';
		this.configModel.shopId = data['yooShopId'] || '';
		this.configModel.secretKey = data['yooSecretKey'] || '';
	}
	
	ngOnDestroy()
	{
		this._ngDestroy$.next();
		this._ngDestroy$.complete();
	}
}
