import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	OnDestroy,
}                                from '@angular/core';
import {
	FormControl,
	FormGroupDirective,
	NgForm,
	FormBuilder,
}                                from '@angular/forms';
import { ErrorStateMatcher }     from '@angular/material/core';
import { MatDialog }             from '@angular/material/dialog';
import { Router }                from '@angular/router';
import { TranslateService }      from '@ngx-translate/core';
import { Subject }               from 'rxjs';
import { first }                 from 'rxjs/operators';
import { InviteRequestRouter }   from '@modules/client.common.angular2/routers/invite-request-router.service';
import { InviteRouter }          from '@modules/client.common.angular2/routers/invite-router.service';
import { CustomerRouter }        from '@modules/client.common.angular2/routers/customer-router.service';
import { CustomerAuthRouter }    from '@modules/client.common.angular2/routers/customer-auth-router.service';
import { MessagePopUpComponent } from 'app/shared/message-pop-up/message-pop-up.component';
import { StorageService }        from 'app/services/storage';
import { environment as env }    from 'environments/environment';
import { styleVariables }        from 'styles/variables';
import { ToolbarController }     from '../app.component';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher
{
	isErrorState(
			control: FormControl | null,
			form: FormGroupDirective | NgForm | null
	): boolean
	{
		const isSubmitted = form && form.submitted;
		return !!(
				control &&
				control.invalid &&
				(control.dirty || control.touched || isSubmitted)
		);
	}
}

// tslint:disable-next-line:max-classes-per-file
@Component({
	           selector:    'login',
	           styleUrls:   ['./login.component.scss'],
	           templateUrl: './login.component.html',
           })
export class LoginComponent implements ToolbarController, OnInit, OnDestroy
{
	public readonly endpoints = {
		http:  env.HTTP_SERVICES_ENDPOINT,
		https: env.HTTPS_SERVICES_ENDPOINT
	}
	public readonly authUrl = {
		yandex:    env.YANDEX_APP_URL,
		google:    env.GOOGLE_APP_URL,
		facebook:  env.FACEBOOK_APP_URL,
		vkontakte: env.VKONTAKTE_APP_URL
	}
	public readonly styleVariables: typeof styleVariables = styleVariables;
	
	public msgAllowGPS: string = 'TO_BE_INVITED_ALLOW_GPS';
	
	public confirmPopUpButton: string = 'OK';
	public commonPopUpText: string = 'WRONG_CODE_TRY_AGAIN';
	public modalTitleText: string = 'CONFIRMATION';
	
	public readonly toolbarDisabled = true;
	
	public formControl = this.fb.group({
		                                   code: [''],
	                                   });
	
	public matcher = new MyErrorStateMatcher();
	public inviteAddress: string | null = null;
	public authLogo = env.AUTH_LOGO;
	private code = this.formControl.get('code');
	private _ngDestroy$ = new Subject<void>();
	@ViewChild('codeRef', { read: ElementRef })
	private codeRef: ElementRef;
	
	constructor(
			private readonly router: Router,
			private readonly inviteRequestRouter: InviteRequestRouter,
			private readonly fb: FormBuilder,
			protected inviteRouter: InviteRouter,
			protected customerRouter: CustomerRouter,
			private translateService: TranslateService,
			private dialog: MatDialog,
			private customerAuthRouter: CustomerAuthRouter,
			private storage: StorageService
	)
	{
		this.addressLoad();
	}
	
	public ngOnInit()
	{
		this.onCodeInputChange();
		return;
	}
	
	public ngOnDestroy()
	{
		this._ngDestroy$.next();
		this._ngDestroy$.complete();
	}
	
	public get isInvited(): boolean
	{
		return (this.storage.inviteRequestId && this.storage.inviteRequestId.length > 0);
	}
	
	public openInvalidInviteCodeDialog(): void
	{
		const dialogRef = this.dialog.open(MessagePopUpComponent, {
			width: '560px',
			data:  {
				modalTitle:    this.modalTitleText,
				confirmButton: this.confirmPopUpButton,
				commonText:    this.commonPopUpText,
			},
		});
	}
	
	public openMsgAllowGPSDialog(): void
	{
		this.commonPopUpText = 'TO_BE_INVITED_ALLOW_GPS';
		const dialogRef = this.dialog.open(MessagePopUpComponent, {
			width: '560px',
			data:  {
				modalTitle:    this.modalTitleText,
				confirmButton: this.confirmPopUpButton,
				commonText:    this.commonPopUpText,
			},
		});
	}
	
	public getTranslate(key: string): string
	{
		let translationResult = '';
		this.translateService.get(key).subscribe((res) => translationResult = res);
		return translationResult;
	}
	
	public loadByLocation()
	{
		this.router.navigate(['login/byLocation']);
	}
	
	private onCodeInputChange()
	{
		this.code
		    .valueChanges
		    .subscribe((code) =>
		               {
			               if(this.code.value !== '')
			               {
				               if(code >= 1000 && code <= 9999)
				               {
					               this.onCodeInserted();
				               }
				               if(code > 9999)
				               {
					               this.code.setValue(`${this.code.value}`.slice(4, 5));
				               }
			               }
		               });
	}
	
	private async onCodeInserted()
	{
		this.codeRef.nativeElement.querySelector('input').readOnly = true;
		navigator.geolocation.getCurrentPosition(
				async({ coords }) =>
				{
					const [longitude, latitude] = [
						coords.longitude,
						coords.latitude,
					];
					const invite = await this.inviteRouter
					                         .getByCode({
						                                    location:   {
							                                    type:        'Point',
							                                    coordinates: [longitude, latitude],
						                                    },
						                                    inviteCode: this.code.value,
					                                    })
					                         .pipe(first())
					                         .toPromise();
					this.codeRef
					    .nativeElement
					    .querySelector('input').readOnly = false;
					
					if(invite != null)
					{
						await this.register(invite);
					}
					else
					{
						this.openInvalidInviteCodeDialog();
						
						this.code.setValue('');
					}
				},
				(err) =>
				{
					this.openMsgAllowGPSDialog();
					this.code.setValue('');
				}
		);
	}
	
	private async register(invite)
	{
		const user = await this.customerAuthRouter.register({
			                                                user: {
			                                                	username: '',
				                                                email: '',
				                                                apartment: invite.apartment,
				                                                geoLocation: invite.geoLocation,
			                                                },
		                                                });
		
		this.storage.customerId = user.id;
		await this.router.navigate(['products']);
	}
	
	private addressLoad()
	{
		if(localStorage.inviteRequestId)
		{
			this.inviteRequestRouter
			    .get(localStorage.inviteRequestId)
			    .subscribe((result) =>
			               {
				               const address = result['geoLocation']['streetAddress'];
				               const houseNumber = `${result['geoLocation']['house']}${
						               result['apartment'] !== '0'
						               ? '/' + result['apartment']
						               : ''
				               }`;
				               const city = result['geoLocation']['city'];
				               this.inviteAddress = `${address} ${houseNumber}, ${city}`;
			               });
		}
	}
}
