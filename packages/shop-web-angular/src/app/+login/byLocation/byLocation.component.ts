import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { HttpClient }                                 from '@angular/common/http';
import { Router, ActivatedRoute }                     from '@angular/router';
import { first, map }                                 from 'rxjs/operators';
import { styleVariables }                             from 'styles/variables';
import { ICustomerCreateObject }                      from '@modules/server.common/interfaces/ICustomer';
import { ILocation }                                  from '@modules/server.common/interfaces/IGeoLocation';
import Customer                                       from '@modules/server.common/entities/Customer';
import GeoLocation                                    from '@modules/server.common/entities/GeoLocation';
import Invite                                         from '@modules/server.common/entities/Invite';
import InviteRequest                                  from '@modules/server.common/entities/InviteRequest';
import { GeoLocationRouter }                          from '@modules/client.common.angular2/routers/geo-location-router.service';
import { InviteRouter }                               from '@modules/client.common.angular2/routers/invite-router.service';
import { InviteRequestRouter }                        from '@modules/client.common.angular2/routers/invite-request-router.service';
import { CustomerRouter }                             from '@modules/client.common.angular2/routers/customer-router.service';
import { CustomerAuthRouter }                         from '@modules/client.common.angular2/routers/customer-auth-router.service';
import { LocationFormComponent }                      from './location/location.component';
import { StorageService }                             from 'app/services/storage';
import { GeoLocationService }                         from 'app/services/geo-location';
import { environment }                                from 'environments/environment';

export interface ICustomerDto
{
	customer: ICustomerCreateObject;
	password?: string;
}

@Component({
	           selector:    'es-login-by-location',
	           styleUrls:   ['./byLocation.component.scss'],
	           templateUrl: './byLocation.component.html',
           })
export class LoginByLocationComponent implements OnInit
{
	@ViewChild('locationForm')
	public locationForm: LocationFormComponent;
	
	public InitUser: Customer;
	public userId: string;
	public email: string;
	
	public mapCoordEmitter = new EventEmitter<google.maps.LatLng | google.maps.LatLngLiteral>();
	public mapGeometryEmitter = new EventEmitter<any>();
	
	public readonly styleVariables: typeof styleVariables = styleVariables;
	
	public readonly toolbarDisabled = true;
	
	public authLogo = environment.AUTH_LOGO;
	
	public coordinates: ILocation;
	
	constructor(
			protected inviteRouter: InviteRouter,
			protected inviteRequestRouter: InviteRequestRouter,
			protected http: HttpClient,
			protected router: Router,
			protected userRouter: CustomerRouter,
			private activatedRoute: ActivatedRoute,
			private geoLocationRouter: GeoLocationRouter,
			private storage: StorageService,
			private userAuthRouter: CustomerAuthRouter,
			private geoLocationService: GeoLocationService
	)
	{
		this.loadUserData();
	}
	
	ngOnInit()
	{
		this.updateCurrentAddressByCoordinates();
	}
	
	async login(): Promise<void>
	{
		if(this.userId)
		{
			const readyUser =
					      await this.userRouter
					                .updateCustomer(this.userId, {
						                username:                this.locationForm.username,
						                email:                   this.locationForm.email,
						                geoLocation:             this.locationForm
						                                             .getCreateUserInfo().geoLocation,
						                socialIds:               this.InitUser
						                                         ? this.InitUser.socialIds
						                                         : [],
						                isRegistrationCompleted: true,
						                apartment:               this.locationForm.apartament
						                                         ? this.locationForm.apartament.toString()
						                                         : '0',
					                });
			this.storage.customerId = readyUser.id;
			await this.router.navigate(['products']);
		}
		else
		{
			const invite = await this.inviteRouter
			                         .getByLocation({
				                                        apartment:     this.locationForm.apartament ?? '0',
				                                        house:         this.locationForm.house,
				                                        streetAddress: this.locationForm.streetAddress,
				                                        city:          this.locationForm.city,
				                                        countryId:     this.locationForm.countryId,
			                                        })
			                         .pipe(first())
			                         .toPromise();
			if(invite != null)
			{
				await this.register(invite);
			}
			else
			{
				const inviteRequest = await this.locationForm.createInviteRequest();
				if(this.storage.inviteSystem)
				{
					await this.processInviteRequest(inviteRequest);
				}
				else
				{
					const inviteCurrent = await this.inviteRouter.create(
							inviteRequest
					);
					await this.register(inviteCurrent);
				}
			}
			
		}
	}
	
	public onCoordinatesChanges(coords: number[])
	{
		this.mapCoordEmitter.emit({ lat: coords[0], lng: coords[1] });
	}
	
	public onGeometrySend(geometry: any)
	{
		this.mapGeometryEmitter.emit(geometry);
	}
	
	private async register(invite: Invite)
	{
		const geo: GeoLocation = invite.geoLocation;
		const user = await this.userAuthRouter
		                       .register({
			                                 user:     {
				                                 username:    this.locationForm.username,
				                                 email:       this.locationForm.email,
				                                 apartment:   invite.apartment,
				                                 geoLocation: invite.geoLocation,
			                                 },
			                                 password: this.locationForm.password
		                                 });
		
		this.storage.customerId = user.id;
		
		await this.router.navigate(['products']);
	}
	
	private async updateCurrentAddressByCoordinates(): Promise<boolean>
	{
		try
		{
			const coords = await this.geoLocationService.getCurrentCoords();
			this.coordinates = {
				type:        'Point',
				coordinates: [coords.latitude, coords.longitude],
			};
		} catch(error)
		{
			console.error(error);
			return false;
		}
		
		return true;
	}
	
	private async processInviteRequest(
			inviteRequest: InviteRequest
	): Promise<void>
	{
		this.storage.inviteSystem
		if(inviteRequest)
		{
			this.storage.inviteRequestId = inviteRequest.id;
		}
		else
		{
			// TODO: show here that we can't get location and can't send invite because of that...
		}
		await this.router.navigate(['login']);
	}
	
	private async loadUserData()
	{
		this.userId = await this.activatedRoute
		                        .params
		                        .pipe(
				                        map(p => p.id),
				                        first()
		                        )
		                        .toPromise();
		if(this.userId)
		{
			this.InitUser = await this.userRouter
			                          .get(this.userId)
			                          .pipe(first())
			                          .toPromise();
			
			this.email = this.InitUser.email;
		}
	}
}
