import { Component, Input, OnInit }           from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Comment                                from '@modules/server.common/entities/Comment';
import { ProductCommentService }              from 'app/services/product-comment.service';

@Component({
	           selector:    'es-product-comment',
	           templateUrl: './product-comment.component.html',
	           styleUrls:   ['./product-comment.component.scss']
           })
export class ProductCommentComponent implements OnInit
{
	@Input()
	public warehouseProductId: string;
	
	@Input()
	public warehouseId: string;
	
	@Input()
	public customerId: string;
	
	public comments: Comment[] = [];
	
	public commentForm: FormGroup;
	
	constructor(
			private fb: FormBuilder,
			private productCommentService: ProductCommentService
	)
	{}
	
	public get message(): string
	{
		return this.commentForm.value.message;
	}
	
	public ngOnInit()
	{
		this.commentForm = this.fb.group({ message: ['', Validators.required] });
		
		if(this.warehouseId && this.warehouseProductId)
		{
			this.productCommentService
			    .getComments(this.warehouseId, this.warehouseProductId)
			    .subscribe((comments) =>
			               {
				               if(comments)
					               this.comments = comments;
			               });
		}
		
	}
	
	public onCommentCancel()
	{
		this.commentForm.reset();
	}
	
	public onSubmit()
	{
		if(this.message && this.message.length > 0)
		{
			this.productCommentService
			    .addComment(
					    this.warehouseId,
					    this.warehouseProductId,
					    {
						    message:   this.message,
						    userId:    this.customerId,
						    productId: this.warehouseProductId
					    }
			    )
			    .then(comments =>
			          {
				          console.debug(comments);
				          this.onCommentCancel();
			          });
		}
	}
	
}
