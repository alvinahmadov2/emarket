import {
	Component,
	ViewChild,
	Inject,
	OnInit
}                                        from '@angular/core';
// import 'style-loader!leaflet/dist/leaflet.css';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import CustomerOrder                     from '@modules/server.common/entities/CustomerOrder';

declare var google: any;
const directionsDisplay = new google.maps.DirectionsRenderer();
const directionsService = new google.maps.DirectionsService();

@Component({
	           selector:    'ea-carrier-location',
	           styleUrls:   ['./carrier-location.component.scss'],
	           templateUrl: './carrier-location.component.html',
           })
export class CarrierLocationComponent implements OnInit
{
	@ViewChild('gmap', { static: true })
	public gmapElement: any;
	public map: google.maps.Map;
	public marker: any;
	public userMarker: any;
	public warehouseMarker: any;
	public carrierLoc: any;
	public storeLoc: any;
	public userOrder: CustomerOrder;
	
	constructor(
			private dialogRef: MatDialogRef<CarrierLocationComponent>,
			@Inject(MAT_DIALOG_DATA)
					data
	)
	{
		this.carrierLoc = data.carrier;
		this.storeLoc = data.merchant;
		this.userOrder = data.userOrder;
	}
	
	public ngOnInit(): void
	{
		this.showMap();
		this.showIconsOnMap();
	}
	
	public showIconsOnMap()
	{
		const newCoordinates = new google.maps.LatLng(
				this.carrierLoc.geoLocation.coordinates.lat,
				this.carrierLoc.geoLocation.coordinates.lng
		);
		
		const warehouseIcon =
				      'https://maps.google.com/mapfiles/kml/pal3/icon21.png';
		const userIcon = 'https://maps.google.com/mapfiles/kml/pal3/icon48.png';
		
		this.userMarker = this.addMarker(
				new google.maps.LatLng(
						this.userOrder.geoLocation.coordinates.lat,
						this.userOrder.geoLocation.coordinates.lng
				),
				this.map,
				userIcon
		);
		
		this.warehouseMarker = this.addMarker(
				new google.maps.LatLng(
						this.storeLoc.geoLocation.coordinates.lat,
						this.storeLoc.geoLocation.coordinates.lng
				),
				this.map,
				warehouseIcon
		);
		const start = new google.maps.LatLng(
				this.userOrder.geoLocation.coordinates.lat,
				this.userOrder.geoLocation.coordinates.lng
		);
		const end = new google.maps.LatLng(
				this.storeLoc.geoLocation.coordinates.lat,
				this.storeLoc.geoLocation.coordinates.lng
		);
		const request = {
			origin:      start,
			destination: end,
			travelMode:  'DRIVING',
		};
		
		directionsService.route(request, function(res, stat)
		{
			if(stat === 'OK')
			{
				directionsDisplay.setDirections(res);
			}
		});
		
		directionsDisplay.setOptions({
			                             suppressMarkers: true,
		                             });
		directionsDisplay.setMap(this.map);
		
		const warehouseInfoContent = `
									<h3>  ${this.storeLoc.name}</h3>
									<ul>
										<li>
											<i style='margin-right:5px;' class="ion-md-mail"></i>
											${this.storeLoc.contactEmail}
										</li>
										<li>
											<i style='margin-right:5px;' class="ion-md-phone"></i><i class="ion-md-call"></i>
											${this.storeLoc.contactPhone}
										</li>
										<li>
											<i style='margin-right:5px;' class="ion-md-locate"></i>
											${this.storeLoc.geoLocation.streetAddress}
										</li>
									</ul>
									`;
		
		const warehouseInfoWindow = new google.maps.InfoWindow({
			                                                       content: warehouseInfoContent,
		                                                       });
		
		this.warehouseMarker.addListener('click', () =>
		{
			warehouseInfoWindow.open(this.map, this.warehouseMarker);
		});
		
		this.map.setCenter(newCoordinates);
		const carierIcon =
				      'https://maps.google.com/mapfiles/kml/pal4/icon54.png';
		
		this.marker = this.addMarker(newCoordinates, this.map, carierIcon);
		const carrierInfoContent = `
					<h3>  ${this.carrierLoc.fullName}</h3>
					<ul>
						<li>${this.carrierLoc.username}</li>
						<li><i style='margin-right:5px;' class="ion-md-call"></i>${this.carrierLoc.phone}</li>
						<li><i style='margin-right:5px;' class="ion-md-locate"></i>${this.carrierLoc.geoLocation.streetAddress}</li>
					</ul>
					`;
		
		const carrierInfoWindow = new google.maps.InfoWindow({
			                                                     content: carrierInfoContent,
		                                                     });
		
		this.marker.addListener('click', () =>
		{
			carrierInfoWindow.open(this.map, this.marker);
		});
	}
	
	public revertMap()
	{
		this.map.setZoom(15);
		this.warehouseMarker.setMap(null);
		this.userMarker.setMap(null);
	}
	
	public showMap()
	{
		const mapProp = {
			center:    new google.maps.LatLng(42.642941, 23.334149),
			zoom:      15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		};
		this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
	}
	
	public addMarker(position, map, icon)
	{
		return new google.maps.Marker({
			                              position,
			                              map,
			                              icon,
		                              });
	}
}
