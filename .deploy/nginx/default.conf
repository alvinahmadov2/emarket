upstream backend {
    server 0.0.0.0:5500      max_fails=3 fail_timeout=30s;
}

upstream api {
    server 0.0.0.0:5555      max_fails=3 fail_timeout=30s;
}

upstream subscriptions {
    server 0.0.0.0:5050      max_fails=3 fail_timeout=30s;
}

upstream socket_nodes {
    ip_hash;
    server 0.0.0.0:5500;
}

server {
	listen 5500;

	location / {
			if ($request_method = 'OPTIONS') {
    			add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                # Custom headers and headers various browsers *should* be OK with but aren't
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                # Tell client that this pre-flight info is valid for 20 days
    			add_header 'Access-Control-Max-Age' 1728000;
    			add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
    		}
            if ($request_method = 'POST') {
                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
    		}
            if ($request_method = 'GET') {
    			add_header 'Access-Control-Allow-Origin' '*' always;
    			add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
    			add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
    			add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
    		}
	}
}

server {
	listen 80 default_server;
	listen [::]:80 default_server;
    root /home/emarket/packages/shop-web-angular/www;

    location / {
#     	rewrite ^/shop/ /$1 break;
		try_files $uri /index.html =404;
		proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
    }

#     location ~ ^/core/(?<myarg>[a-zA-Z_.?]*)\/? {
# 		rewrite /core/^ /$myarg break;
# 		set $url "$myarg?$query_string";
# 		proxy_pass $scheme://backend/$url;
# 		proxy_redirect off;
# 		proxy_http_version 1.1;
# 		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
# 		proxy_set_header X-Real-IP $remote_addr;
# 	}

	# Requests for socket.io are passed on to Node on backend
#     location ~* /\.io {
#           proxy_pass $scheme://socket_nodes;
#           proxy_http_version 1.1;
#           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#           proxy_set_header Host $host;
#           proxy_set_header Upgrade $http_upgrade;
#           proxy_set_header Connection "upgrade";
# 	}

	location ^~ /socket.io/ {
            proxy_pass $scheme://0.0.0.0:5500;
            proxy_redirect off;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
    }

	 location ^~ /api {
	    rewrite ^/api/ /$1 break;
		proxy_pass $scheme://api$request_uri;
		proxy_redirect off;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		add_header X-Debug-Message "host: $host; remote_addr: $remote_addr; backend_request_uri: $scheme://backend$request_uri; proxy_add_x_forwarded_for: $proxy_add_x_forwarded_for; http_upgrade: $http_upgrade;" always;
	}

	location = /api/graphql {
		rewrite ^/api/graphql/ /$1 break;
		proxy_pass $scheme://api/graphql;
		proxy_redirect off;
		add_header X-Debug-Message "host: $host; remote_addr: $remote_addr; backend_request_uri: $scheme://backend$request_uri; proxy_add_x_forwarded_for: $proxy_add_x_forwarded_for; http_upgrade: $http_upgrade;" always;
	}

	 location = /api/subscriptions {
		rewrite ^/api/subscriptions/ /$1 break;
		proxy_redirect off;
		proxy_pass $scheme://subscriptions;
		proxy_http_version 1.1;
	}
}
